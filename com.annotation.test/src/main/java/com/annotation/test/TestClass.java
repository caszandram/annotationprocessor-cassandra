package com.annotation.test;


import com.annotation.*;

import java.lang.reflect.*;



@PrintMe
public abstract class TestClass {
	public TestClass(){
		
	}
}


class Class1{
	public Class1()
	{
		
	}
	
	@PrintMe
	public String toMethod(@PrintMe String str){			
			return  str;
	}
}

@PrintMe
class Class2  {
	public Class2(){
		
	}
}

@PrintMe
class Class3 {
	  public Class3(String as) {
	  }

	  public Class3(int ai) {
	  }

	  public Class3() {
	  }
}

@PrintMe
class Class4{
	public Class4() {
	}
}

@PrintMe
class Class5  {
	public Class5() {
	}
}
