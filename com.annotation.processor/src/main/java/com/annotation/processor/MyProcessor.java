package com.annotation.processor;

import static javax.lang.model.SourceVersion.RELEASE_7;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import com.annotation.PrintMe;

@SupportedAnnotationTypes({"com.annotation.PrintMe"}) //it processes only @PrintMe annotations
@SupportedSourceVersion(RELEASE_7)
public class MyProcessor extends AbstractProcessor
{
	@Override
   public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) 
   //method (declared in the Processor interface) in a series of rounds(process() invocations)
   // to process a subset of the annotations found on the source and class files produced by a prior round -- 
   //rounds are needed because new annotations can be introduced in a given round and they need to be processed
	
   {
   Set<? extends Element> elements;
	  elements = roundEnv.getElementsAnnotatedWith(PrintMe.class); 
	  Iterator<? extends Element> iter = elements.iterator(); 
	  
		ArrayList<Element> classes = new ArrayList<Element>();
		ArrayList<Element> methods = new ArrayList<Element>();
		ArrayList<Element> param = new ArrayList<Element>();
	  
      while (iter.hasNext())
      {  
     	 Element element = iter.next();		      	
              
         if (element.getKind() == ElementKind.CLASS)
         {	            		   	            	
             note("classes that has annotation @PrintMe: " +element);
            classes.add(element); 

     	   try {  
     		        		   
                File file = new File("PrintClasses.txt");
                BufferedWriter output = new BufferedWriter(new FileWriter(file));
                output.write("Classes that has annotation @PrintMe \n");
                output.write("\n" +classes);
                output.close();
                
              } catch ( IOException e ) {
                 e.printStackTrace();
              }     
          
         }
           	                    
         if (element.getKind() == ElementKind.METHOD){
             note("Method that has annotation @PrintMe: " +element);
	            
	               methods.add(element);
         	  
         	   try {  
	        		  
	        		   
	                   File file = new File("PrintMethod.txt");
	                   BufferedWriter output = new BufferedWriter(new FileWriter(file));
	                   output.write("Method that has annotation @PrintMe \n");
	                   output.write("\n" +methods );
	                   output.close();
	                   
	                 } catch ( IOException e ) {
	                    e.printStackTrace();
	                 
         }
         }
         
         if(element.getKind() == ElementKind.PARAMETER){
         	 note("Parameter that has annotation @PrintMe: " +element);
		            
	               param.add(element);
         	  
         	   try {  
	        		  	        		   
	                   File file = new File("PrintParameter.txt");
	                   BufferedWriter output = new BufferedWriter(new FileWriter(file));
	                   output.write("Parameter that has annotation @PrintMe \n");
	                   output.write("\n" +param );
	                   output.close();
	                   
	                 } catch ( IOException e ) {
	                    e.printStackTrace();
	                 
	                 }
         	}
   
      }
      	return true;
	}

   

	void note(String msg)
	{
	   processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, msg);	      	      
	  
	}


}
